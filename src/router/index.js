import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import Contador from '../views/Contador.vue'
import Pestanas from '../views/Pestanas.vue'
import Ecuaciones from '../views/Ecuaciones.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/contador',
    name: 'contador',
    component: Contador
  },
  {
    path: '/pestanas',
    name: 'pestanas',
    component: Pestanas
  },
  {
    path: '/ecuaciones',
    name: 'ecuaciones',
    component: Ecuaciones
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
