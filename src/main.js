import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/es'

Vue.use(ElementUI, { locale });

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')
